<?php

    class Admin extends CI_Controller {

        public function __construct() {
            parent::__construct();

            if (    !isset($_SESSION["tesi2022"]) ) {
                header("location: /index.php/login");
            }
        }

        public function index() {
            $this->load->model("adminModel");

            $admin = $this->adminModel->selecionarTodos();
            $tabela = "";

            //echo "Bem vindo " . @$_SESSION["tesi2022"]["email"];

            foreach($admin as $item ) {
                //GET
                $tabela = $tabela . "<tr>";

                if ( isset($_SESSION["tesi2022"])) {
                    $tabela = $tabela . "
                        <td style='cursor: pointer'>
                            <a href='/index.php/admin/alterar?codigo=" . $item->id . "'>
                                ✏️
                            </a>
                            <a href='/index.php/admin/excluir?codigo=" . $item->id . "'>
                                ❌    
                            </a>
                        </td>";
                }

                $tabela = $tabela . "
                        <td>" . $item->nome ."</td>
                        <td>" . $item->perecivel ."</td>
                        <td>" . $item->valor ."</td>
                        <td>" . $item->nome_tipo ."</td>
                        <td>
                            <img src='" . $item->imagem . "' style='width:150px' />
                        </td>
                    </tr>
                ";
            }

            $variavel = array(
                "lista_admin" => $admin,
                "tabela" => $tabela,
                "titulo" => "Você está em Marquinhos admin",
                "sucesso" => "admin add com sucesso",
                "erro" => "sdadasda"
            );

            $this->template->load("templates/adminTemp", "admin/index", $variavel );
        }

        //Alteração de veículo
        public function alterar() {
            $this->load->model("adminModel");

            $id = $_GET["codigo"];

            $retorno = $this->adminModel->buscarId( $id );
            
            $data = array(
                "titulo"=>"Alteração de produto",
                "admin"=>$retorno[0],
                "opcoes"=>$this->produtoTipo($retorno[0]->nome_tipo) //3, 4, 5
            );

            $this->template->load("templates/adminTemp", "admin/formAlterar", $data);


        }

        //Salva os dados atualizados 
        public function salvaralteracao() {
            $this->load->model("adminModel");

            $id = $_POST["id"];
            $nome = $_POST["nome"];
            $perecivel = $_POST["perecivel"];
            $valor = $_POST["valor"];
            $nome_tipo = $_POST["nome_tipo"];
            $imagem = $_POST["imagem"];
           
            $retorno = $this->adminModel->salvaraltercao($id,$nome, $perecivel, $valor, $nome_tipo, $imagem);

            if ($retorno == true) {
                header('location: /index.php/admin');
            }
            else {
                echo "houve erro na alteração";
            }
        }
        
        //Criar admin
        public function formNovo() {

            $opcao = $this->produtoTipo( 0 );
            
            $data = array(
                'opcoes' => $opcao
            );

            $this->template->load("templates/adminTemp","/admin/formnovo", $data);
        }

        private function produtoTipo( $idproduto ) {
            $this->load->model("produtoModel");
            $tipo = $this->produtoModel->selecionarTodos();

            $option = "";
            foreach($tipo as $linha) {
                $selecionado = "";

                if ( $idproduto == $linha->id )
                    $selecionado = "selected";


                $option .= "<option 
                                value='" . $linha->id . "'
                                " . $selecionado . "
                            >" 
                                . $linha->nome_tipo . 
                            "</option>"; 
            }

            return $option;
        }

        //Salvar novo admin
        public function salvarnovo() {
            
            $this->load->model("adminModel");
            $nome = $_POST["nome"];
            $perecivel = $_POST["perecivel"];
            $valor = $_POST["valor"];
            $nome_tipo = $_POST["nome_tipo"];
            $imagem = $_POST["imagem"];
            
           


            $retorno = $this->adminModel->buscarModelo( $nome );

            //var_dump( $_POST );

            if ( $retorno[0]->total > 0 ) {
                echo "Não pode incluir, já existe um total de " . $retorno[0]->total;
            } else {
                $retorno = $this->adminModel->salvarnovo(
                    $nome,$perecivel, $valor,$nome_tipo, $imagem
                ); 
                
                header("location: /index.php/admin");
            }
        }

        //Excluir 
        public function excluir() {
            $this->load->model("adminModel");

            $id = $_GET["codigo"];

            $this->adminModel->excluir($id);

            header("location: /index.php/admin");
        }

    }
?>