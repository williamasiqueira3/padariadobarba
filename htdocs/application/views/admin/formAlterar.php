<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $titulo; ?></title>
</head>
<body>
    <form method="POST" action="/index.php/admin/salvaralteracao">
        <input type="hidden" name="id" value="<?php echo $admin->id; ?>"/>

        <label>Produto</label>
        <input type="text" name="nome" value="<?php echo $admin->nome; ?>"/>
        <br/>

        <label>Perecivel</label>
        <input type="text" name="perecivel" value="<?php echo $admin->perecivel; ?>"/>
        <br />
   
        <label>Valor</label>
        <input type="text" name="valor" value="<?php echo $admin->valor; ?>"/>
        <br />

        <label>Tipo Produto</label>
        <select name="nome_tipo" required>
            <option value="">Selecione Opção</option>
            <?php echo $opcoes; ?>
        </select>
        <br />
        
        <label>Imagem</label>
        <input type="text" name="imagem" value="<?php echo $admin->imagem; ?>"/>
        <br />
        
        <br />
        <input type="submit" value="Salvar" />
        <a href='/index.php/admin'>Voltar/Cancelar</a>

    </form>
</body>
</html>